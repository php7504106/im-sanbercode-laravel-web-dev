<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome', [
        'name' => 'Rezky'
    ]);
});

Route::get('/master', function() 
{
    return view('layouts.master');
});
Route::get('/table', function() 
{
    return view('table');
});
Route::get('/data-tables', function() 
{
    return view('data-tables');
});

Route::get('/cast', [CastController::class, 'index']);

//Create
Route::get('/cast/create', [CastController::class, 'create']);
//
Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{cast_id}', [CastController::class, 'detail']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

Route::put('/cast/{cast_id}', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
