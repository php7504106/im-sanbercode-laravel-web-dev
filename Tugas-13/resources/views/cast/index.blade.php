@extends('layouts.master')

@section('title')
    <h1>Cast</h1>
@endsection

@section('sub-title')
    <h3>Daftar Cast</h3>
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary">Tambah Cast</a>

    <div class="containern mt-4">
        <div class="row">
            @forelse ($casts as $cast)
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img src="https://source.unsplash.com/500x400?face" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"> Nama : {{ $cast->name }}</h5>
                            <p class="card-text">Umur : {{ $cast->umur }}</p>
                            <form action="/cast/{{ $cast->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/cast/{{ $cast->id }}" class="btn btn-primary btn-sm">Detail</a>
                                <a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                            </form>
                        </div>
                    </div>
                </div>
            @empty
                
            @endforelse
            
        </div>
    </div>
@endsection