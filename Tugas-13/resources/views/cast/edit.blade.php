@extends('layouts.master')

@section('title')
    <h1>Cast</h1>
@endsection

@section('sub-title')
    <h3>Edit Data Cast {{ $cast->name }}</h3>
@endsection

@section('content')
    <form action="/cast/{{ $cast->id }}" method="post">
        @csrf
        @method('Put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="name" value="{{ $cast->name }}">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name="umur" value="{{ $cast->umur }}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control" name="bio" rows="3" >{{ $cast->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="submit" class="btn btn-primary" value="Update">
    </form>
@endsection