@extends('layouts.master')

@section('title')
    <h1>Tambah Data</h1>
@endsection

@section('sub-title')
    <h3>Masukan Data Cast</h3>
@endsection

@section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="name">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control" name="bio" rows="3"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="submit" class="btn btn-primary" value="Simpan Data">
    </form>
@endsection