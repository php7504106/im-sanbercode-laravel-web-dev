@extends('layouts.master')

@section('title')
    <h1>Cast</h1>
@endsection

@section('sub-title')
    <h3>Detail Cast : {{ $cast->name }}</h3>
@endsection

@section('content')
    {{-- <a href="/cast/create" class="btn btn-primary">Tambah Cast</a>

    <div class="containern mt-4">
        <div class="row">
            @forelse ($casts as $cast)
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img src="https://source.unsplash.com/500x400?face" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title"> Nama : {{ $cast->name }}</h5>
                          <p class="card-text">Umur : {{ $cast->umur }}</p>
                          <a href="/cast/{{ $cast->id }}" class="btn btn-primary">Detail</a>
                        </div>
                      </div>
                </div>
            @empty
                
            @endforelse
            
        </div>
    </div> --}}
    <div class="d-flex justify-content-center">
        <div class="card" style="width: 18rem;">
            <img src="https://source.unsplash.com/500x400?face" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Nama : {{ $cast->name }}</h5>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Umur : {{ $cast->umur }}</li>
                <li class="list-group-item"> Bio : {{ $cast->bio }} </li>
            </ul>
        </div>
    </div>

@endsection