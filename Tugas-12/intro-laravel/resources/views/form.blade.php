<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Formulir Pendaftaran</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
      <label>First Name :</label> <br /><br />
      <input type="text" name="firstname" /> <br /><br />
      <label>Last Name :</label> <br /><br />
      <input type="text" name="lastname" /> <br /><br />
      <label>Gender:</label> <br /><br />
      <input type="radio" name="gender" />Male <br />
      <input type="radio" name="gender" />Female <br />
      <input type="radio" name="gender" />Other <br />
      <br />
      <label>Nationality:</label> <br />
      <br />
      <select name="nationality">
        <option value="indoensia">Indonesia</option>
        <option value="WNA">WNA</option>
      </select>
      <br /><br />
      <label>Languange Spoken</label><br /><br />
      <input type="checkbox" name="language" value="bahasa" /> Bahasa Indonesia <br />
      <input type="checkbox" name="language" value="inggris" /> Bahasa Inggris <br />
      <input type="checkbox" name="language" value="other" /> Other <br />
      <br />
      <label>Bio</label><br /><br />
      <textarea name="bio" id="" cols="30" rows="10"></textarea><br /><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
