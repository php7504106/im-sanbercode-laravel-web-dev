<?php

require_once "Animal.php";
require_once "Ape.php";
require_once "Frog.php";

//release 0
$goat = new Animal("Shaun");
echo "Name : $goat->name";
echo "<br>";
echo "Legs : $goat->legs";
echo "<br>";
echo "Cold Blooded : $goat->cold_blooded";
echo "<br>";
echo "<br>";

$frog = new Frog("Buduk");
echo "Name : $frog->name";
echo "<br>";
echo "Legs : $frog->legs";
echo "<br>";
echo "Cold Blooded : $frog->cold_blooded";
echo "<br>";
echo "Jump : " .$frog->jump();
echo "<br>";
echo "<br>";

$ape = new Ape("Kera Sakti");
echo "Name : $ape->name";
echo "<br>";
echo "Legs : $ape->legs";
echo "<br>";
echo "Cold Blooded : $ape->cold_blooded";
echo "<br>";
echo "Yell : " . $ape->yell();
echo "<br>";